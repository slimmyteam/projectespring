import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResolvedicesComponent } from './resolvedices.component';

describe('ResolvedicesComponent', () => {
  let component: ResolvedicesComponent;
  let fixture: ComponentFixture<ResolvedicesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResolvedicesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ResolvedicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
