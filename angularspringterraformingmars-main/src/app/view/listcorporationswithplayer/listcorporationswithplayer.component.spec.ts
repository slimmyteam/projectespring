import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListcorporationswithplayerComponent } from './listcorporationswithplayer.component';

describe('ListcorporationswithplayerComponent', () => {
  let component: ListcorporationswithplayerComponent;
  let fixture: ComponentFixture<ListcorporationswithplayerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListcorporationswithplayerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListcorporationswithplayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
