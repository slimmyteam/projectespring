import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {Dices} from "../classes/dices";

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private dataSubject = new BehaviorSubject<Dices>({corporation:1,dices:[0,0,0,0,0,0]});
  public data$ = this.dataSubject.asObservable();

  updateData(newData: Dices) {
    this.dataSubject.next(newData);
  }
  constructor() { }
}
