import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetwinnergameComponent } from './setwinnergame.component';

describe('SetwinnergameComponent', () => {
  let component: SetwinnergameComponent;
  let fixture: ComponentFixture<SetwinnergameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetwinnergameComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SetwinnergameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
