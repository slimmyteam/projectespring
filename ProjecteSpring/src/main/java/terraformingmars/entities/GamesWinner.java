package terraformingmars.entities;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "GamesWinner")
public class GamesWinner {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idGamesWinner", updatable = false, insertable = false)
	private int idGamesWinner;

	@Column
	private LocalDateTime dateEnd = LocalDateTime.now();

	@ManyToOne
	@JoinColumn(name = "idWinner")
	private Players player;

	@OneToOne(cascade = CascadeType.ALL) //puede tener el JsonManagedReference
	private Games game;

	public GamesWinner() {
		super();
	}

	public GamesWinner(LocalDateTime dateEnd) {
		super();
		this.dateEnd = dateEnd;
	}

	public LocalDateTime getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(LocalDateTime dateEnd) {
		this.dateEnd = dateEnd;
	}
	
	public Players getPlayer() {
		return player;
	}

	public void setPlayer(Players player) {
		this.player = player;
	}

	public Games getGame() {
		return game;
	}

	public void setGame(Games game) {
		this.game = game;
	}

	public void setIdGamesWinner(int idGamesWinner) {
		this.idGamesWinner = idGamesWinner;
	}

	public int getIdGamesWinner() {
		return idGamesWinner;
	}

	@Override
	public String toString() {
		return "GamesWinner [idGamesWinner=" + idGamesWinner + ", dateEnd=" + dateEnd + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idGamesWinner;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GamesWinner other = (GamesWinner) obj;
		if (idGamesWinner != other.idGamesWinner)
			return false;
		return true;
	}

}
