package terraformingmars.utils;

public class Resultat {

	private String estat;

	public Resultat() {
		super();
	}

	public Resultat(String estat) {
		super();
		this.estat = estat;
	}

	public String getEstat() {
		return estat;
	}

	public void setEstat(String estat) {
		this.estat = estat;
	}
}
