import { Component } from '@angular/core';
import {ConnectService} from "./shared/services/connect-service.service";
import {DataService} from "./shared/services/data-service.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'AngularSpring';
  dices!: number[];
  constructor(private dataService: DataService){};
  ngOnInit() {
    this.dataService.data$.subscribe((data) => {
      this.dices = data.dices;
    });
  }
}
