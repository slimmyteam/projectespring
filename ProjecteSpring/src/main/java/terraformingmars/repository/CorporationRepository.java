package terraformingmars.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import terraformingmars.entities.Corporations;

@Repository
public interface CorporationRepository extends JpaRepository<Corporations, Integer> {

	List<Corporations> findAllByOrderByVictoryPointsDesc();
}
