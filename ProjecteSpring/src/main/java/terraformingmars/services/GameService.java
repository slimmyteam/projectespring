package terraformingmars.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import terraformingmars.entities.Games;
import terraformingmars.repository.GameRepository;

@Service
public class GameService {

	@Autowired
	GameRepository gameRepository;

	public List<Games> findAll() {
		return gameRepository.findAll();
	}

	public Games update(Games g) {
		if (gameRepository != null) {
			return gameRepository.save(g);
		}
		return null;
	}
}
