package terraformingmars.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import terraformingmars.entities.Games;

@Repository
public interface GameRepository extends JpaRepository<Games, Integer> {

}
