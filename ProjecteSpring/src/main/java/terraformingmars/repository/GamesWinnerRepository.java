package terraformingmars.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import terraformingmars.entities.GamesWinner;

@Repository
public interface GamesWinnerRepository extends JpaRepository<GamesWinner, Integer> {
	
}
