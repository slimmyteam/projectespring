package terraformingmars.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import terraformingmars.entities.Players;

@Repository
public interface PlayerRepository extends JpaRepository<Players, Integer> {

}
