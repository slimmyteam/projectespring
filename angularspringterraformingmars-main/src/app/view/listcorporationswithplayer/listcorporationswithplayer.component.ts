import { Component } from '@angular/core';
import {ConnectService} from "../../shared/services/connect-service.service";
import {CorporationsService} from "../../shared/services/corporations-service.service";
import {Corporations} from "../../shared/classes/corporations";

@Component({
  selector: 'app-listcorporationswithplayer',
  templateUrl: './listcorporationswithplayer.component.html',
  styleUrls: ['./listcorporationswithplayer.component.css']
})
export class ListcorporationswithplayerComponent {
  message!:string;
  corporations !: Corporations;
  ngOnInit() {
    this.corps.data$.subscribe((data) => {
      this.corporations = data;
    });
  }
  constructor(private connectbd: ConnectService, private corps : CorporationsService){};
  resposta() {
    this.connectbd.getCorporationsWithPlayer().subscribe(res => {
      this.message = "";
      console.log(res);
      if (res.length > 0){
        for(let i = 0; i < res.length; i++){
          this.message += "<p>Id Corporació: " + res[i].corporation.idCorporations + ", Nom corporació: "+ res[i].corporation.name +", Nom player: " + res[i].name + "<p/>";
        }
        this.corps.updateData(this.corporations);
      } else {
        this.message = "<p>No hi ha elements.</p>";
      }
    });
  }
}
