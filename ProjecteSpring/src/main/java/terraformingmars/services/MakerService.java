package terraformingmars.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import terraformingmars.entities.Corporations;
import terraformingmars.entities.Makers;
import terraformingmars.entities.typemaker;
import terraformingmars.repository.MakerRepository;

@Service
public class MakerService {

	@Autowired
	MakerRepository makerRepository;

	public List<Makers> findAll() {
		return makerRepository.findAll();
	}

	public Makers update(Makers m) {
		return makerRepository.save(m);
	}

	public List<Makers> getMakersByTypeWithoutCorporation(typemaker typemaker) {
		return makerRepository.findByTypemakerAndCorporationIsNull(typemaker);
	}

	public List<Makers> getMakersByTypeWithCorporation(typemaker typemaker) {
		return makerRepository.findByTypemakerAndCorporationIsNotNull(typemaker);
	}

	public List<Makers> getMakersByTypeAndCorporation(typemaker typemaker, Corporations idCorporation) {
		return makerRepository.findByTypemakerAndCorporation(typemaker, idCorporation);
	}
}
