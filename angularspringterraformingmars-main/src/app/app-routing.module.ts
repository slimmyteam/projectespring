import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CommonModule} from "@angular/common";
import {AliveComponent} from "./view/alive/alive.component";
import {DicesComponent} from "./view/dices/dices.component";
import {ResolvedicesComponent} from "./view/resolvedices/resolvedices.component";
import {EndgameComponent} from "./view/endgame/endgame.component";
import {
  ListmakersbytypewithoutcorporationComponent
} from "./view/listmakersbytypewithoutcorporation/listmakersbytypewithoutcorporation.component";
import {SetwinnergameComponent} from "./view/setwinnergame/setwinnergame.component";
import {SetvictorypointswinnersComponent} from "./view/setvictorypointswinners/setvictorypointswinners.component";
import {SetvictorypointsmakersComponent} from "./view/setvictorypointsmakers/setvictorypointsmakers.component";
import {
  ListmakersbytypewithcorporationComponent
} from "./view/listmakersbytypewithcorporation/listmakersbytypewithcorporation.component";
import {
  ListcorporationswithplayerComponent
} from "./view/listcorporationswithplayer/listcorporationswithplayer.component";

const routes: Routes = [
  {path : 'Alive', component : AliveComponent },
  {path : 'Dices', component : DicesComponent },
  {path : 'listMakersNoCorporation', component : ListmakersbytypewithoutcorporationComponent },
  {path : 'listMakersCorporation', component : ListmakersbytypewithcorporationComponent },
  {path : 'listCorporationPlayers', component : ListcorporationswithplayerComponent },
  {path : 'ResolveDices', component : ResolvedicesComponent },
  {path : 'IsEndGame', component : EndgameComponent },
  {path : 'PointsMakers', component : SetvictorypointsmakersComponent },
  {path : 'PointsWinners', component : SetvictorypointswinnersComponent },
  {path : 'SetWinner', component : SetwinnergameComponent },
];

@NgModule({
  imports: [CommonModule, RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
