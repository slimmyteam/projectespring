import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AliveComponent } from './view/alive/alive.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import { DicesComponent } from './view/dices/dices.component';
import { ResolvedicesComponent } from './view/resolvedices/resolvedices.component';
import { EndgameComponent } from './view/endgame/endgame.component';
import { ListmakersbytypewithoutcorporationComponent } from './view/listmakersbytypewithoutcorporation/listmakersbytypewithoutcorporation.component';
import { SetvictorypointsmakersComponent } from './view/setvictorypointsmakers/setvictorypointsmakers.component';
import { SetvictorypointswinnersComponent } from './view/setvictorypointswinners/setvictorypointswinners.component';
import { SetwinnergameComponent } from './view/setwinnergame/setwinnergame.component';
import { ListmakersbytypewithcorporationComponent } from './view/listmakersbytypewithcorporation/listmakersbytypewithcorporation.component';
import { ListcorporationswithplayerComponent } from './view/listcorporationswithplayer/listcorporationswithplayer.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    AliveComponent,
    DicesComponent,
    ResolvedicesComponent,
    EndgameComponent,
    ListmakersbytypewithoutcorporationComponent,
    SetvictorypointsmakersComponent,
    SetvictorypointswinnersComponent,
    SetwinnergameComponent,
    ListmakersbytypewithcorporationComponent,
    ListcorporationswithplayerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
