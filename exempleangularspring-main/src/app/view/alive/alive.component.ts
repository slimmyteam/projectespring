import { Component } from '@angular/core';
import {ConnectService} from "../../shared/services/connect.service";

@Component({
  selector: 'app-alive',
  templateUrl: './alive.component.html',
  styleUrls: ['./alive.component.css']
})
export class AliveComponent {
  message!:string;
  constructor(private connectbd: ConnectService){};
  resposta() {
    this.connectbd.getAlive().subscribe(res => {
      this.message = res.estat;

    });
  }
}
