package terraformingmars.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import terraformingmars.entities.Players;
import terraformingmars.repository.PlayerRepository;
@Service
public class PlayerService {
	
	@Autowired
	PlayerRepository playerRepository;
	
	public List<Players> getCorporationsWithPlayer() {
		return playerRepository.findAll();
	}
}
