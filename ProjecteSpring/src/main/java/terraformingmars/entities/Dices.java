package terraformingmars.entities;

import java.util.Arrays;

public class Dices {

	int[] dices = new int[6];
	int idCorporation;

	public Dices() {
		super();
	}

	public Dices(int[] dices) {
		super();
		this.dices = dices;
	}

	public Dices(int[] dices, int idCorporation) {
		this(dices);
		this.idCorporation = idCorporation;
	}

	public int[] getDados() {
		return dices;
	}

	public void setDados(int[] dices) {
		this.dices = dices;
	}

	public int getIdCorporation() {
		return idCorporation;
	}

	public void setIdCorporation(int idCorporation) {
		this.idCorporation = idCorporation;
	}

	public String recogerDados() {
		return "La corporación con id " + idCorporation + " ha sacado los siguientes dados: " + dices.toString();
	}

	@Override
	public String toString() {
		return "Dices [dices=" + Arrays.toString(dices) + ", idCorporation=" + idCorporation + "]";
	}

}
