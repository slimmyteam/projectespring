package terraformingmars.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import terraformingmars.entities.Corporations;
import terraformingmars.repository.CorporationRepository;

@Service
public class CorporationService {

	@Autowired
	CorporationRepository corporationRepository;

	public List<Corporations> findAll() {
		return corporationRepository.findAll();
	}

	public Corporations update(Corporations c) {
		return corporationRepository.save(c);
	}

	public List<Corporations> getCorporationOrderedByVictoryPoints() {
		return corporationRepository.findAllByOrderByVictoryPointsDesc();
	}
}
