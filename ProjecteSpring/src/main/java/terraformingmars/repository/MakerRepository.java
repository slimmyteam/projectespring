package terraformingmars.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import terraformingmars.entities.Corporations;
import terraformingmars.entities.Makers;
import terraformingmars.entities.typemaker;

@Repository
public interface MakerRepository extends JpaRepository<Makers, Integer> {
	
	List<Makers> findByTypemakerAndCorporationIsNull(typemaker typemaker);

	List<Makers> findByTypemakerAndCorporationIsNotNull(typemaker typemaker);
	
	List<Makers> findByTypemakerAndCorporation(typemaker typemaker, Corporations idCorporation);
}
