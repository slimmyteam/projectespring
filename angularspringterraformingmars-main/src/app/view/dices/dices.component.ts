import {Component, EventEmitter, Output} from '@angular/core';
import {ConnectService} from "../../shared/services/connect-service.service";
import {DataService} from "../../shared/services/data-service.service";

@Component({
  selector: 'app-dices',
  templateUrl: './dices.component.html',
  styleUrls: ['./dices.component.css']
})
export class DicesComponent {
  message!:string;

  constructor(private connectbd: ConnectService, private dataService: DataService){};
  resposta() {
    this.connectbd.getDices().subscribe(res => {
      this.message = res.dados;
      this.dataService.updateData({corporation:res.corporation, dices: res.dados});
    });
  }
}
