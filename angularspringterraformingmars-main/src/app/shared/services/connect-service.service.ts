import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Dices} from "../classes/dices";
import {typemaker} from "../classes/typemaker";

@Injectable({
  providedIn: 'root'
})
export class ConnectService {

  REST_API: string = 'http://localhost:9000';
  constructor(private httpclient : HttpClient) {}
  public getAlive(): Observable<any>{
    return this.httpclient.get(`${this.REST_API}/`);
  }

  public getDices(): Observable<any>{
    return this.httpclient.get(`${this.REST_API}/rollingDices`);
  }
  public postResolveDices(dices:Dices): Observable<any>{
    return this.httpclient.post(`${this.REST_API}/resolveDices`,dices);
  }
  public getIsEndGame(): Observable<any>{
    return this.httpclient.get(`${this.REST_API}/isEndGame`);
  }
  public getMakersByTypeWithoutCorporation(typemaker: typemaker | undefined): Observable<any>{
    return this.httpclient.get(`${this.REST_API}/getMakersByTypeWithoutCorporation?typemaker=`+ typemaker );
  }

  public getMakersByTypeWithCorporation(typemaker: typemaker | undefined, idcorporation: number): Observable<any>{
    return this.httpclient.get(`${this.REST_API}/getMakersByTypeWithCorporation?typemaker=`+
      typemaker + `&idCorporation=` + idcorporation);
  }

  public getCorporationsWithPlayer(): Observable<any>{
    return this.httpclient.get(`${this.REST_API}/getCorporationsWithPlayer`);
  }

  public postSetVictoryPointsMakers(): Observable<any>{
    return this.httpclient.post(`${this.REST_API}/setVictoryPointsMakers`,"");
  }

  public postSetVictoryPointsWinners(): Observable<any>{
    return this.httpclient.post(`${this.REST_API}/setVictoryPointsWinners`,"");
  }

  public postSetWinnerGame(): Observable<any>{
    return this.httpclient.post(`${this.REST_API}/setWinnerGame`,"");
  }
}
