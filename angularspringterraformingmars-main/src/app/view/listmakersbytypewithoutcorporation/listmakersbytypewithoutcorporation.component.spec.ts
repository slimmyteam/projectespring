import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListmakersbytypewithoutcorporationComponent } from './listmakersbytypewithoutcorporation.component';

describe('ListmakersbytypewithoutcorporationComponent', () => {
  let component: ListmakersbytypewithoutcorporationComponent;
  let fixture: ComponentFixture<ListmakersbytypewithoutcorporationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListmakersbytypewithoutcorporationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListmakersbytypewithoutcorporationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
