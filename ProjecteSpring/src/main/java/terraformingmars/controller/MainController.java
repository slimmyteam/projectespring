package terraformingmars.controller;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import terraformingmars.entities.Corporations;
import terraformingmars.entities.Dices;
import terraformingmars.entities.Games;
import terraformingmars.entities.GamesWinner;
import terraformingmars.entities.Makers;
import terraformingmars.entities.Players;
import terraformingmars.entities.typemaker;
import terraformingmars.services.CorporationService;
import terraformingmars.services.GameService;
import terraformingmars.services.GamesWinnerService;
import terraformingmars.services.MakerService;
import terraformingmars.services.PlayerService;
import terraformingmars.utils.Resultat;

//URL para consultar apis localhost:9000/swagger-ui.html
@RestController
@RequestMapping(path = "/")
public class MainController {

	@Autowired
	CorporationService corporationService;

	@Autowired
	GameService gameService;

	@Autowired
	GamesWinnerService gamesWinnerService;

	@Autowired
	MakerService makerService;

	@Autowired
	PlayerService playerService;

	static List<Players> players;
	static Random r = new Random();
	static Dices dados;

	@GetMapping(path = "/")
	public ResponseEntity<?> welcome() {

		return ResponseEntity.ok(new Resultat("Hola, Angular funciona"));
	}

	@GetMapping(path = "/rollingDices")
	public ResponseEntity<?> rollingDices() {
		int[] dices = new int[6];
		Random r = new Random();
		for (int i = 0; i < 6; i++) {
			dices[i] = r.nextInt(1, 7);
		}
		Dices dados = new Dices(dices);
		this.dados = dados;
		return ResponseEntity.ok(dados);
	}

	@GetMapping(path = "/getCorporationsWithPlayer")
	public ResponseEntity<?> getCorporationsWithPlayer() {
		List<Players> players = playerService.getCorporationsWithPlayer();
		this.players = players;
		this.dados.setIdCorporation(players.get(0).getCorporation().getIdCorporations());
		return ResponseEntity.ok(players);
	}

	@GetMapping(path = "/getMakersByTypeWithoutCorporation")
	public ResponseEntity<?> getMakersByTypeWithoutCorporation(@RequestParam typemaker typemaker) {
		List<Makers> makers = makerService.getMakersByTypeWithoutCorporation(typemaker);
		return ResponseEntity.ok(makers);
	}

	@GetMapping(path = "/getMakersByTypeWithCorporation")
	public ResponseEntity<?> getMakersByTypeWithCorporation(@RequestParam typemaker typemaker) {
		List<Makers> makers = makerService.getMakersByTypeWithCorporation(typemaker);
		return ResponseEntity.ok(makers);
	}

	@PostMapping(path = "/resolveDices")
	public ResponseEntity<?> resolveDices(Dices dados) {
		Games g = new Games();
		String resultat = TirarDados(g);
		return ResponseEntity.ok(new Resultat(resultat));
	}

	private static String TirarDados(Games g) {
		GameService gameService = new GameService();
		MakerService makerService = new MakerService();
		CorporationService corporationService = new CorporationService();

		int contTemp = 0;
		int contOxi = 0;
		int contOcea = 0;
		int contBosc = 0;
		int contCiutat = 0;
		int contPunts = 0;
		String resultat = "";

		resultat += "La tirada de dados es: " + dados.toString() + ". ";
		for (int i = 0; i < 6; i++) {
			int dado = dados.getDados()[i];
			switch (dado) {
			case 1:
				contTemp++;
				if (contTemp == 3) {
					g.setTemperature(g.getTemperature() + 2);
					resultat += "Aumentamos la temperatura 2ºC. ";
					contTemp = 0;
					gameService.update(g);
				}
				break;
			case 2:
				contOxi++;
				if (contOxi == 3) {
					g.setOxygen(g.getOxygen() + 1);
					resultat += "Aumentamos el oxígeno en 1. ";
					contOxi = 0;
					gameService.update(g);
				}
				break;
			case 3:
				contOcea++;
				if (contOcea == 3) {
					resultat += "La " + players.get(0).getCorporation().getName() + " del " + players.get(0).getName()
							+ " ha ganado un maker de océano. ";
					contOcea = 0;
				}
				break;
			case 4:
				contBosc++;
				if (contBosc == 4) {
					resultat += "La " + players.get(0).getCorporation().getName() + " del " + players.get(0).getName()
							+ " ha ganado un maker de bosque. ";
					contBosc = 0;
				}
				break;
			case 5:
				contCiutat++;
				if (contCiutat == 3) {
					resultat += "La " + players.get(0).getCorporation().getName() + " del " + players.get(0).getName()
							+ " ha ganado un maker de ciudad. ";
					contCiutat = 0;
				}
				break;
			case 6:
				contPunts++;
				if (contPunts == 3) {
					players.get(0).getCorporation()
							.setVictoryPoints(players.get(0).getCorporation().getVictoryPoints() + 2);
					contPunts = 0;
					resultat += "La " + players.get(0).getCorporation().getName() + " del " + players.get(0).getName()
							+ " ha ganado 2 puntos. ";
				}
				break;
			}
		}
		System.out.println("@@TEMPERATURA: " + g.getTemperature());
		System.out.println("@@OXÍGENO: " + g.getOxygen());

		return resultat;
	}

	@GetMapping(path = "/isEndGame")
	public ResponseEntity<?> getIsEndGame() {
		List<Games> games = gameService.findAll();
		List<Makers> listaMakersOceans = makerService.getMakersByTypeWithoutCorporation(typemaker.OCEA);
		String resultat = ComprobarIndicadores(games.get(0), listaMakersOceans);
		System.out.println(resultat);
		return ResponseEntity.ok(new Resultat(resultat));
	}

	private static String ComprobarIndicadores(Games g, List<Makers> listaMakersOceans) {
		String resultat = "";
		MakerService makerService = new MakerService();
		int contCondicions = 0;

		if (g.getTemperature() >= 0) {
			resultat += "Se ha cumplido la condición: la temperatura es igual o mayor a 0. ";
			contCondicions++;
		}
		if (g.getOxygen() >= 14) {
			resultat += "Se ha cumplido la condición: el oxígeno es igual o mayor a 14. ";
			contCondicions++;
		}
//		List<Makers> listaMakersOceans = makerService.getMakersByTypeWithoutCorporation(typemaker.OCEA);

		if (listaMakersOceans.size() == 0) {
			resultat += "Se ha cumplido la condición: todos los oceanos tienen un jugador asignado. ";
			contCondicions++;
		}

		if (contCondicions >= 2) {
			resultat += "Se han cumplido 2 condiciones. ";
			resultat += "\n*************FIN DE LA PARTIDA*************\n ";

		} else {
			resultat = "No se han cumplido las condiciones.";
		}

		return resultat;
	}

	@PostMapping(path = "/setVictoryPointsMakers")
	public ResponseEntity<?> setVictoryPointsMakers() {
		List<Players> players = playerService.getCorporationsWithPlayer();
		String resultat = "";
		for (Players player : players) {
			Corporations corp = player.getCorporation();
			Set<Makers> makers = corp.getMakers();
			corp.setVictoryPoints(corp.getVictoryPoints() + makers.size());
			resultat += "La corporación " + corp.getName() + " ha obtenido " + makers.size()
					+ " puntos y en total tiene " + corp.getVictoryPoints() + ".\r\n";
			corporationService.update(corp);
		}

		return ResponseEntity.ok(new Resultat(resultat));
	}

	@PostMapping(path = "/setVictoryPointsWinners")
	public ResponseEntity<?> setVictoryPointsWinners() {
		List<Players> players = playerService.getCorporationsWithPlayer();
		System.out.println("Player 1: " + players.get(0));
		String resultat = "";
		for (Players player : players) {
			List<Makers> makersOceans = makerService.getMakersByTypeAndCorporation(typemaker.OCEA,
					player.getCorporation());

			if (makersOceans.size() >= 3) {
				player.getCorporation().setVictoryPoints(player.getCorporation().getVictoryPoints() + 3);
			}

			corporationService.update(player.getCorporation());
		}

		List<Makers> makersBoscsPlayer1 = makerService.getMakersByTypeAndCorporation(typemaker.BOSC,
				players.get(0).getCorporation());
		List<Makers> makersBoscsPlayer2 = makerService.getMakersByTypeAndCorporation(typemaker.BOSC,
				players.get(1).getCorporation());
		List<Makers> makersBoscsPlayer3 = makerService.getMakersByTypeAndCorporation(typemaker.BOSC,
				players.get(2).getCorporation());
		List<Makers> makersBoscsPlayer4 = makerService.getMakersByTypeAndCorporation(typemaker.BOSC,
				players.get(3).getCorporation());

		List<Makers> makersCiutatsPlayer1 = makerService.getMakersByTypeAndCorporation(typemaker.CIUTAT,
				players.get(0).getCorporation());
		List<Makers> makersCiutatsPlayer2 = makerService.getMakersByTypeAndCorporation(typemaker.CIUTAT,
				players.get(1).getCorporation());
		List<Makers> makersCiutatsPlayer3 = makerService.getMakersByTypeAndCorporation(typemaker.CIUTAT,
				players.get(2).getCorporation());
		List<Makers> makersCiutatsPlayer4 = makerService.getMakersByTypeAndCorporation(typemaker.CIUTAT,
				players.get(3).getCorporation());

		ArrayList<Integer> listaCiutatsPunts = new ArrayList<Integer>();
		listaCiutatsPunts.add(makersCiutatsPlayer1.size());
		listaCiutatsPunts.add(makersCiutatsPlayer2.size());
		listaCiutatsPunts.add(makersCiutatsPlayer3.size());
		listaCiutatsPunts.add(makersCiutatsPlayer4.size());

		// Comparator para ordenar los puntos
		Comparator<Integer> comparatorPunts = new Comparator<Integer>() {

			public int compare(Integer o1, Integer o2) {
				return o2 - o1;
			}
		};

		listaCiutatsPunts.sort(comparatorPunts);

		ArrayList<Integer> listaBoscPunts = new ArrayList<Integer>();
		listaBoscPunts.add(makersBoscsPlayer1.size());
		listaBoscPunts.add(makersBoscsPlayer2.size());
		listaBoscPunts.add(makersBoscsPlayer3.size());
		listaBoscPunts.add(makersBoscsPlayer4.size());

		listaBoscPunts.sort(comparatorPunts);

		List<Integer> sublistCiutatsPunts = listaCiutatsPunts.subList(0, 2);
		List<Integer> sublistBoscPunts = listaBoscPunts.subList(0, 2);

		// ASIGNAR FIRST WINNER
		List<Corporations> corporacions = corporationService.findAll();
		int limitador = 0;
		while (!sublistCiutatsPunts.isEmpty() && limitador < 200) {
			limitador++;
			for (Corporations corp : corporacions) {
				if (!sublistCiutatsPunts.isEmpty() && sublistCiutatsPunts.get(0) == makerService
						.getMakersByTypeAndCorporation(typemaker.CIUTAT, corp).size()) {
					if (sublistCiutatsPunts.get(0) == 0) {
						break;
					} else if (sublistCiutatsPunts.size() == 2) {
						corp.setVictoryPoints(corp.getVictoryPoints() + 5);
						resultat += "La corporación " + corp.getName()
								+ " consigue 5 puntos por ser la primera en tener más makers de " + typemaker.CIUTAT
								+ ". ";
					} else {
						corp.setVictoryPoints(corp.getVictoryPoints() + 3);
						resultat += "La corporación " + corp.getName()
								+ " consigue 3 puntos por ser la segunda en tener más makers de " + typemaker.CIUTAT
								+ ". ";
					}
					sublistCiutatsPunts.remove(0);
				}
			}
		}
		limitador = 0;

		// ASIGNAR SECOND WINNER
		corporacions = corporationService.findAll();
		while (!sublistBoscPunts.isEmpty() && limitador < 200) {
			limitador++;
			for (Corporations corp : corporacions) {
				if (!sublistBoscPunts.isEmpty() && sublistBoscPunts.get(0) == makerService
						.getMakersByTypeAndCorporation(typemaker.BOSC, corp).size()) {
					if (sublistBoscPunts.get(0) == 0) {
						break;
					} else if (sublistBoscPunts.size() == 2) {
						corp.setVictoryPoints(corp.getVictoryPoints() + 5);
						resultat += "La corporación " + corp.getName()
								+ " consigue 5 puntos por ser la primera en tener más makers de " + typemaker.BOSC
								+ ". ";
					} else {
						corp.setVictoryPoints(corp.getVictoryPoints() + 3);
						resultat += "La corporación " + corp.getName()
								+ " consigue 3 puntos por ser la segunda en tener más makers de " + typemaker.BOSC
								+ ". ";
					}
					sublistBoscPunts.remove(0);
				}
			}
		}

		return ResponseEntity.ok(new Resultat(resultat));
	}

	@PostMapping(path = "/setWinnerGame")
	public ResponseEntity<?> setWinnerGame() {
		List<Players> players = playerService.getCorporationsWithPlayer();
		List<Corporations> listaCorporaciones = corporationService.getCorporationOrderedByVictoryPoints();
		String resultat = "La corporación ganadora es " + listaCorporaciones.get(0).getName() + " con "
				+ listaCorporaciones.get(0).getVictoryPoints() + " puntos. ";

		for (Players player : players) {
			if (player.getCorporation().getIdCorporations() == listaCorporaciones.get(0).getIdCorporations()) {
				GamesWinner gw = new GamesWinner();
				gw.setIdGamesWinner(player.getIdPlayer());
				gamesWinnerService.update(gw);
				resultat += "Y el player ganador es " + player.getName() + ". ";
			}
		}

		return ResponseEntity.ok(new Resultat(resultat));
	}
}
