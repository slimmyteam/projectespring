import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AliveComponent} from "./view/alive/alive.component";
import {ListalljugadoresComponent} from "./view/listalljugadores/listalljugadores.component";

const routes: Routes = [
  {path : 'Alive', component : AliveComponent },
  {path : 'AllJugadores', component : ListalljugadoresComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
