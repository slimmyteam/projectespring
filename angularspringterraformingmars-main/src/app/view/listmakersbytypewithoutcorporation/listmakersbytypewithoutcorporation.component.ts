import { Component } from '@angular/core';
import {ConnectService} from "../../shared/services/connect-service.service";
import {typemaker} from "../../shared/classes/typemaker";

@Component({
  selector: 'app-listmakersbytypewithoutcorporation',
  templateUrl: './listmakersbytypewithoutcorporation.component.html',
  styleUrls: ['./listmakersbytypewithoutcorporation.component.css']
})
export class ListmakersbytypewithoutcorporationComponent {
  typemaker = [typemaker.OCEA, typemaker.CIUTAT, typemaker.BOSC];
  selectedTypeMakers: typemaker | undefined;
  message!: string;
  constructor(private connectbd: ConnectService){};
  resposta() {
    this.connectbd.getMakersByTypeWithoutCorporation(this.selectedTypeMakers).subscribe(res => {
      this.message="";
      console.log(res);
      if (res.length > 0){
        for(let i = 0; i < res.length; i++){
          this.message += "<p>Id: " + res[i].idMakers + ", name: " + res[i].name + "<p/>";
        }
      } else {
        this.message = "<p>No hi ha elements.</p>";
      }

    });
  }
}
