import {typemaker} from "./typemaker";

export interface Makers {
  idMakers: number,
  name: string,
  maxNeighbours: number,
  typemaker: typemaker,
  corporation: number
}
