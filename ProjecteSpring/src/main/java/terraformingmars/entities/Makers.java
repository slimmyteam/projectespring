package terraformingmars.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "Makers")
public class Makers implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Idmakers", updatable = false, insertable = false)
	private Integer idMakers;

	@Column(name = "Name", length = 20, nullable = false)
	private String name;

	@Column(name = "Maxneighbours")
	private int maxNeighbours = 0; // valor per defecte

	@Enumerated(EnumType.STRING)
	@Column(name = "typemaker")
	private typemaker typemaker;

	@ManyToOne
	@JoinColumn(name = "IdCorporation(FK)")
	@JsonManagedReference
	private Corporations corporation;

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "Makers_NeighbourMaker", joinColumns = @JoinColumn(name = "IdMaker"), inverseJoinColumns = @JoinColumn(name = "IdNeighbourMaker"))
	@JsonBackReference
	private Set<Makers> makers;

	@ManyToMany(cascade = CascadeType.ALL, mappedBy = "makers")
	@JsonBackReference
	private Set<Makers> neighbourMakers;

	public Makers() {
		super();
		this.makers = new HashSet<Makers>();
	}

	public Makers(String name, typemaker typeMaker) {
		this();
		this.name = name;
		this.typemaker = typeMaker;
	}

	public Makers(String name, typemaker typeMaker, int maxNeighbours) {
		this(name, typeMaker);
		this.maxNeighbours = maxNeighbours;
	}

	public int getIdMakers() {
		return idMakers;
	}

	public void setIdMakers(int idMakers) {
		this.idMakers = idMakers;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMaxNeighbours() {
		return maxNeighbours;
	}

	public void setMaxNeighbours(int maxNeighbours) {
		this.maxNeighbours = maxNeighbours;
	}

	public typemaker getTypeMaker() {
		return typemaker;
	}

	public void setTypeMaker(typemaker typeMaker) {
		this.typemaker = typeMaker;
	}

	public Corporations getCorporation() {
		return corporation;
	}

	public void setCorporation(Corporations corporation) {
		this.corporation = corporation;
	}

	public Set<Makers> getMakers() {
		return makers;
	}

	public void setMakers(Set<Makers> makers) {
		this.makers = makers;
	}

	public Set<Makers> getNeighbourMakers() {
		return neighbourMakers;
	}

	public void setNeighbourMakers(Set<Makers> neighbourMakers) {
		this.neighbourMakers = neighbourMakers;
	}

	@Override
	public String toString() {
		return "Makers [idMakers=" + idMakers + ", name=" + name + ", maxNeighbours=" + maxNeighbours + ", typeMaker="
				+ typemaker + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idMakers;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Makers other = (Makers) obj;
		if (idMakers != other.idMakers)
			return false;
		return true;
	}

}
