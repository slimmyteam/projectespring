package terraformingmars.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import terraformingmars.entities.Corporations;
import terraformingmars.entities.GamesWinner;
import terraformingmars.repository.GamesWinnerRepository;

@Service
public class GamesWinnerService {

	@Autowired
	GamesWinnerRepository gamesWinnerRepository;

	public GamesWinner update(GamesWinner c) {
		return gamesWinnerRepository.save(c);
	}
}
