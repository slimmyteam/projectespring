import { Component } from '@angular/core';
import {ConnectService} from "../../shared/services/connect-service.service";
import {CorporationsService} from "../../shared/services/corporations-service.service";
import {Corporations} from "../../shared/classes/corporations";
import {typemaker} from "../../shared/classes/typemaker";

@Component({
  selector: 'app-listmakersbytypewithcorporation',
  templateUrl: './listmakersbytypewithcorporation.component.html',
  styleUrls: ['./listmakersbytypewithcorporation.component.css']
})
export class ListmakersbytypewithcorporationComponent {
  typemaker = [typemaker.OCEA, typemaker.CIUTAT, typemaker.BOSC];
  selectedTypeMakers: typemaker | undefined;
  message!: string;
  corporations !: Corporations;
  constructor(private connectbd: ConnectService, private corps : CorporationsService){};
  ngOnInit() {
    this.corps.data$.subscribe((data) => {
      this.corporations = data;
    });
  }
  resposta() {
    let corporation:number = this.corporations.idcorporations[Math.floor(Math.random() * 4)];
    this.connectbd.getMakersByTypeWithCorporation(this.selectedTypeMakers, corporation).subscribe(res => {
      this.message="La corporació és " + corporation;
      if (res.length > 0){
        for(let i = 0; i < res.length; i++){
          this.message += "<p>Id: " + res[i].idMakers + ", name: " + res[i].name + "<p/>";
        }
      } else {
        this.message += "<p>No hi ha elements.</p>";
      }

    });
  }
}
